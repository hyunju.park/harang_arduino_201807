# Summary

* [Introduction](README.md)

* [아두이노 시작하기](Part01_시작하기.adoc)

* [아두이노 프로그램 기초](Part02_프로그램기초.adoc)

    * [LED Blink, 버튼, 조도센서](Part02_프로그램기초_1.adoc)
    * [3색 LED, 피에조 부저, 적외선 장애물 감지 센서, 7세그먼트](Part02_프로그램기초_2.adoc)
    * [I2C LCD, 온습도 센서, 초음파센서](Part02_프로그램기초_3.adoc)
    * [DC 모터, 서보모터, 시리얼 통신, 블루투스 통신](Part02_프로그램기초_4.adoc)

* [스마트홈 프로젝트 실습하기](Part03_스마트홈프로젝트실습하기.adoc)

* [아두이노 함수, 클래스, 라이브러리](Part02_프로그램기초_A.adoc)